#include "493Gavenko/Application.hpp"
#include "493Gavenko/Mesh.hpp"
#include "493Gavenko/ShaderProgram.hpp"

#include <iostream>
#include <vector>
#include <algorithm>

class SampleApplication : public Application {
public:
	MeshPtr _shape;
	ShaderProgramPtr _shader;

	// вершины стартовые/удадение/приближение
	unsigned int number_k = 50; 
	unsigned int diff = 2;
	const unsigned int min_k = 10;
	const unsigned int max_k = 1000;
	

	void makeScene() override {
		Application::makeScene();
		_cameraMover = std::make_shared<FreeCameraMover>();

		// радиус и число вершин
		_shape = makeShape(2.f, number_k);
		_shape->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));


		_shader = std::make_shared<ShaderProgram>("493GavenkoData/shaderNormal.vert", "493GavenkoData/shader.frag");
	}

	void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL Sample", nullptr, ImGuiWindowFlags_AlwaysAutoResize))
        {
		if (_shape) {
            		ImGui::Text("new number vertices: %d", _shape->getVertexCount());
		}

        }
        ImGui::End();
    }

	void handleKey(int key, int scancode, int action, int mods) override {
		Application::handleKey(key, scancode, action, mods);

		if (glfGetKey(_window, GLFW_KEY_MINUS) == GLFW_PRESS) {
			number_k = number_k / diff;
			number_k = std::max(number_k, min_k);
			makeScene();
		}
		if (glfGetKey(_window, GLFW_KEY_EQUAL) == GLFW_PRESS) {
			number_k = number_k * diff;
			number_k = std::min(number_k, max_k);
			makeScene();
		}
	}

	void draw() override {
		Application::draw();
		
		int width, height;
		glfwGetFramebufferSize(_window, &width, &height);

		glVieport(0, 0, width, height);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		_shader->use();

		_shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		_shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

		_shader->setMat4Uniform("modelMatrix", _shape->modelMatrix());
		_shape->draw();
	}
};

int main() {
	SampleApplication app;
	app.start();

	return 0;
}
