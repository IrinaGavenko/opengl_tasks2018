#include <Mesh.hpp>

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <iostream>
#include <vector>

// cross-cap : http://virtualmathmuseum.org/Surface/cross-cap/cross-cap.html

// сами координаты
glm::vec3 getPont(double r, double u, double v) {
	return glm::vec3((r*r) * (sin(u) * sin(2*v) / 2),
		(r*r) * (sin(2*u) * cos(v) * cos(v)),
		(r*r) * (cos(2*u) * cos(v) * cos(v)));	
}

// нормаль
glm::vec3 getNormal(double r, double u, double v) {

	glm::vec3 der_u = glm::vec3((r*r) * cos(u) * sin(2*v) / 2,
			2 * (r*r) * cos(2*u) * cos(v) * cos(v),
			(-2) * (r*r) * sin(2*u) * cos(v) * cos(v));

	glm::vec3 der_v = glm::vec3((r*r) * (sin(u) * cos(2*v)),
			(-2) * (r*r) * sin(2*u) * sin(v) * cos(v),
			(-2) * (r*r) * cos(2*u) * sin(v) * cos(v));

	glm::vec3 norm = glm::cross(der_u, der_v);
	return glm::normalize(norm);
}

MeshPtr makeShape(float radius, unsigned int N) {

	std::vector<glm::vec3> vertices;
	std::vector<glm::vec3> normals;	

	// идём по всем вершинам
	for (unsigned int i = 0; i < N; i++) {
		float v = 2. * (double)glm::pi<float>() * i / N;
		float v2 = 2. * (double)glm::pi<float>() * (i + 1) / N;
	
		for (unsigned int j = 0; j < N; j++) {
			float u = 2. * (double)glm::pi<float>() * j / N;
			float u2 = 2. * (double)glm::pi<float>() * (j + 1) / N;

			// первый треугольник, образующий квадрат

			vertices.push_back(getPont(radius, u, v));
			vertices.push_back(getPont(radius, u2, v));
			vertices.push_back(getPont(radius, u, v2));

			vertices.push_back(getNormal(radius, u, v));
			vertices.push_back(getNormal(radius, u2, v));
			vertices.push_back(getNormal(radius, u, v2));

			// второй треугольник, образующий квадрат
			
			vertices.push_back(getPont(radius, u2, v));
			vertices.push_back(getPont(radius, u, v2));
			vertices.push_back(getPont(radius, u2, v2));

			vertices.push_back(getNormal(radius, u, v));
			vertices.push_back(getNormal(radius, u2, v));
			vertices.push_back(getNormal(radius, u, v2));
		}
	}

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());


    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    return mesh;
}
